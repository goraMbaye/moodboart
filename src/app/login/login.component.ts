import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MoodService } from '../mood.service';

export interface DirectionDepart {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {
  direction: string;
  departement: string;
  showSpinner: boolean;
  directions;
  departements;

  constructor(private router: Router, private moodService: MoodService) {}

  ngOnInit() {
    this.showSpinner = false;

    this.moodService.getDirections().subscribe(res => {
      this.directions = res;
    });
  }

  login(): void {
    this.showSpinner = true;
    localStorage.setItem('direction', this.direction);
    localStorage.setItem('department', this.departement);
    this.router.navigate([ '' ]);
  }

  getDep(): void {

    // this.showSpinner = true;

    this.moodService.getDartement({'direction': this.direction}).subscribe(res => {
      this.departements = res;
      // this.showSpinner = false;
    });
  }
}
