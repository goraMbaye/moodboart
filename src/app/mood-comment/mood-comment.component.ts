import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Mood } from '../class/mood';
import { MoodService } from '../mood.service';

const today = new Date();
const day = today.getDate() + '/' + Number(today.getMonth() + 1) + '/' + today.getFullYear();

@Component({
  selector: 'app-mood-comment',
  templateUrl: './mood-comment.component.html',
  styleUrls: ['./mood-comment.component.css']
})
export class MoodCommentComponent implements OnInit {

  content = 0;
  neutre = 0;
  mecontent = 0;

  target = false;
  paramUrl = 'happy';
  src = 'good';
  text = 'J\'espère te revoir demain en pleine forme !';

  moodForm = new FormGroup({
    commentaire: new FormControl(''),
  });

  constructor(private route: ActivatedRoute, private moodService: MoodService, private router: Router) { }

  ngOnInit() {

    this.route.params.subscribe( params => this.paramUrl = params.id );

    if (localStorage.getItem('mooded') === day){
      this.target = true;
    }

    if (this.paramUrl === 'happy') {

      this.content = 1;

    } else if (this.paramUrl === 'unhappy') {

      this.text = 'J\'espère que demain sera meilleur !';
      this.src = 'sad';
      this.mecontent = 1;

    } else if (this.paramUrl === 'neutre') {

      this.text = 'Que cette soirée te donne plus de force !';
      this.src = 'neutral';
      this.neutre = 1;

    } else {

      this.router.navigate([ '/' ]);

    }
  }

  onSubmit() {
    this.target = true;

    let direction = '' + localStorage.getItem('direction') + '';
    let departement = '' + localStorage.getItem('department') + '';

    let mood = new Mood(direction, departement, this.content, this.neutre, this.mecontent, day, this.moodForm.value.commentaire);

    console.log(mood);

    localStorage.setItem('mooded', day);

    this.moodService.addMood(mood).subscribe(res => {
      console.log(res);
    });

  }

}
